#include "macros.dl"

AnyMethodInvocation(?invocation, ?tomethod) :-
  StaticMethodInvocation(?invocation, ?tomethod, _).

AnyMethodInvocation(?invocation, ?tomethod) :-
  VirtualMethodInvocation(?invocation, ?tomethod, _).

AnyMethodInvocation(?invocation, ?tomethod) :-
  MethodInvocation_Method(?invocation, ?tomethod),
  isSpecialMethodInvocation_Insn(?invocation).

VarUse(?from, ?insn) :-
  AssignOper_From(?insn, ?from).

TaintedVarTaintedFromVar(?toCtx, ?to, ?value, ?type) :-
  VarIsTaintedFromVar(?type, ?toCtx, ?to, ?fromCtx, ?from),
  TaintedVarPointsTo(?value, ?fromCtx, ?from).
  .plan 1:(2,1)

TaintedValueTransferred(?declaringType, ?source, ?type, ValueIdMacro(?source, ?type, DEFAULT_BREADCRUMB)),
VarPointsTo(?hctx, ValueIdMacro(?source, ?type, DEFAULT_BREADCRUMB), ?toCtx, ?to) :-
  TaintedVarTaintedFromVar(?toCtx, ?to, ?value, ?type),
  SourceFromTaintedValue(?value, ?source),
  Value_DeclaringType(?value, ?declaringType),
  isImmutableHContext(?hctx).
  .plan 2:(3,1,2,4)

TaintedValueTransferred(?declaringType, ?source, ?type, ValueIdMacro(?source, ?type, DEFAULT_BREADCRUMB)),
VarPointsTo(?hctx, ValueIdMacro(?source, ?type, DEFAULT_BREADCRUMB), ?toCtx, ?to) :-
  SourceFromTaintedValue(?value, ?source),
  Value_DeclaringType(?value, ?declaringType),
  VarIsTaintedFromValue(?type, ?value, ?toCtx, ?to),
  isImmutableHContext(?hctx).

VarIsTaintedFromVar(?type, ?ctx, ?param, ?ctx, ?base) :-
   MethodInvocationInContextInApplication(?ctx, ?invocation, ?method),
   BaseToParamTaintTransferMethod(?method),
   MethodInvocation_Base(?invocation, ?base),
   ActualParam(_, ?invocation, ?param),
   Var_Type(?param, ?type).

VarIsTaintedFromVarIndex(?base, ?ctx, ?param) :-
   MethodInvocationInContext(?ctx, ?invocation, ?method),
   ParamIndexToBaseTaintTransferMethod(?index, ?method),
   MethodInvocation_Base(?invocation, ?base),
   ActualParam(?index, ?invocation, ?param).

// I expect the first rel to be very small, smaller than
// the deltas of VPT or AIPT
VarIsTaintedFromValueIndex(?ctx, ?base, ?hctx, ?value) :-
   VarIsTaintedFromVarIndex(?base, ?ctx, ?param),
   VarPointsTo(?hctx, ?value, ?ctx, ?param).

VarIsTaintedFromValue(?type, ?valueIndex, ?ctx, ?base) :-
  VarIsTaintedFromValueIndex(?ctx, ?base, ?hctx, ?value),
  ArrayIndexPointsTo(_, ?valueIndex, ?hctx, ?value),
  Var_Type(?base, ?type).


CallTaintingMethod(?label, ?ctx, ?invocation) :-
  TaintSourceMethod(?label, ?tomethod),
  MethodInvocationInContext(?ctx, ?invocation, ?tomethod),
  Instruction_Method(?invocation, ?inmethod),
  ApplicationMethod(?inmethod).

/**
 * Information flow through complex relations
 */
.decl AppendableType(?type:Type)

AppendableType("java.lang.Appendable").

AppendableType(?type) :-
   SubtypeOf(?type, ?sft),
   AppendableType(?sft).

StringFactoryType("java.lang.String").

StringFactoryType(?type) :-
   AppendableType(?type).
   


StringFactoryVar(?var) :-
  StringFactoryType(?type),
  Var_Type(?var, ?type).

StringFactoryVarPointsTo(?factoryHctx, ?factoryValue, ?ctx, ?var) :-
  StringFactoryVar(?var),
  VarPointsTo(?factoryHctx, ?factoryValue, ?ctx, ?var).

.decl AppendInvocation_Base(?invocation:MethodInvocation, ?base:Var)

AppendInvocation_Base(?invocation, ?base) :-
  VirtualMethodInvocation_SimpleName(?invocation, "append"),
  VirtualMethodInvocation_Base(?invocation, ?base).

VarIsTaintedFromVar(?type, ?ctx, ?ret, ?ctx, ?base),
VarIsTaintedFromVar(?type, ?ctx, ?ret, ?ctx, ?param) :-
  AppendInvocation_Base(?invocation, ?base),
  StringFactoryVarPointsTo(_, _, ?ctx, ?base),
  AssignReturnValue(?invocation, ?ret),
  Var_Type(?ret, ?type),
  ActualParam(0, ?invocation, ?param).

VarIsTaintedFromVar(?type, ?ctx, ?base, ?ctx, ?param) :-
  AppendInvocation_Base(?invocation, ?base),
  StringFactoryVarPointsTo(_, _, ?ctx, ?base),
  Var_Type(?base, ?type),
  ActualParam(0, ?invocation, ?param).

MethodReturningStringFactory(?tomethod) :-
  StringFactoryType(?stringFactoryType),
  Method_ReturnType(?tomethod, ?stringFactoryType).

StringFactoryReturnInvocation(?invocation) :-
  MethodReturningStringFactory(?tomethod),
  VirtualMethodInvocation(?invocation, ?tomethod, _).

VarIsTaintedFromVar(?type, ?ctx, ?ret, ?ctx, ?base) :-
  StringFactoryVarPointsTo(_, _, ?ctx, ?base),
  VirtualMethodInvocation_Base(?invocation, ?base),
  StringFactoryReturnInvocation(?invocation),
  AssignReturnValue(?invocation, ?ret),
  Var_Type(?ret, ?type).

VarIsTaintedFromValue(?componentType, ?basevalue, ?ctx, ?to) :-
  TaintedValue(?basevalue),
  Value_Type(?basevalue, ?arrayType),
  ComponentType(?arrayType, ?componentType),
  LoadHeapArrayIndex(?ctx, ?to, _, ?basevalue).

VarIsTaintedFromVar(?type, ?ctx, ?ret, ?ctx, ?base) :-
  BaseToRetTaintTransferMethod(?method),
  MethodInvocationInContext(?ctx, ?invocation, ?method),
  MethodInvocation_Base(?invocation, ?base),
  TypeForReturnValue(?type, ?ret, ?invocation).

MethodInvocationInfo(?invocation, ?type, ?ret) :-
  Method_ReturnType(?method, ?type),
  AnyMethodInvocation(?invocation, ?method),
  AssignReturnValue(?invocation, ?ret).

VarIsCast(?var) :-
  AssignCast(_, ?var, _, _).

TypeForReturnValue(?type, ?ret, ?invocation) :-
  OptAssignCast(?type, _, ?ret),
  MethodInvocationInfo(?invocation, _, ?ret).

TypeForReturnValue(?type, ?ret, ?invocation) :-
  MethodInvocationInfo(?invocation, ?type, ?ret),
  !VarIsCast(?ret).

// Taint transfer through aliasing

TaintTransferMethodInvocationInContext(?ctx, ?index, ?invocation) :-
  MethodInvocationInContext(?ctx, ?invocation, ?taintTransferMethod),
  ParamToBaseTaintTransferMethod(?index, ?taintTransferMethod).

ParamTaintTransferredToBase(?param, ?ctx, ?base) :-
  TaintTransferMethodInvocationInContext(?ctx, ?index, ?invocation),
  ActualParam(?index, ?invocation, ?param),
  MethodInvocation_Base(?invocation, ?base),
  !AssignReturnValue(?invocation, _).

MethodInvocationInContextInApplication(?ctx, ?invocation, ?method) :-
  ApplicationMethod(?fromMethod),
  Instruction_Method(?invocation, ?fromMethod),
  MethodInvocationInContext(?ctx, ?invocation, ?method).

ParamTaintTransferredToRet(?type, ?ret, ?ctx, ?param) :-
  ParamToRetTaintTransferMethod(?taintTransferMethod),
  MethodInvocationInContextInApplication(?ctx, ?invocation, ?taintTransferMethod),
  ActualParam(_, ?invocation, ?param),
  TypeForReturnValue(?type, ?ret, ?invocation).

TaintedValueTransferred(?declaringType, ?source, ?type, ValueIdMacro(?source, ?type, DEFAULT_BREADCRUMB)),
VarPointsTo(?hctx, ValueIdMacro(?source, ?type, DEFAULT_BREADCRUMB), ?ctx, ?to) :-
  SourceFromTaintedValue(?value, ?source),
  Value_DeclaringType(?value, ?declaringType),
  TaintedVarPointsTo(?value, ?ctx, ?from),
  ParamTaintTransferredToRet(?type, ?to, ?ctx, ?from),
  isImmutableHContext(?hctx).


#ifdef INFORMATION_FLOW_HIGH_SOUNDNESS
.decl ValueTaintedFromValue(?v1:Value, ?v2:Value)

ValueTaintedFromValue(?newvalue, ?baseObj) :-
  BaseValueTaintedFromParamValue(?newvalue, ?baseObj),
  ?newvalue != ?baseObj.

VarPointsTo(?hctx, ?newvalue, ?ctx, ?var) :-
  ValueTaintedFromValue(?newvalue, ?existingvalue),
  VarPointsTo(_, ?existingvalue, ?ctx, ?var),
  isImmutableHContext(?hctx).
  .plan 1:(2,1,3)

.decl XYZ(?value:Value, ?ctx:Context, ?param:Var)

XYZ(?existingvalue, ?ctx, ?param) :-
  ParamTaintTransferredToBase(?param, ?ctx, ?base),
  VarPointsTo(_, ?existingvalue, ?ctx, ?base).

.decl BaseValueTaintedFromParamValue(v1:Value, v2:Value)

TaintedValueTransferred(?declaringType, ?source, ?type, ValueIdMacro(?source, ?type, DEFAULT_BREADCRUMB)),
BaseValueTaintedFromParamValue(ValueIdMacro(?source, ?type, DEFAULT_BREADCRUMB), ?existingValue) :-
  XYZ(?existingValue, ?ctx, ?param),
  Value_Type(?existingValue, ?type),
  TaintedVarPointsTo(?value, ?ctx, ?param),
  SourceFromTaintedValue(?value, ?source),
  Value_DeclaringType(?value, ?declaringType).
  .plan 1:(2,1,3,4,5)
  .plan 2:(3,1,2,4,5)
  .plan 3:(4,3,1,2,5)
  .plan 4:(5,3,1,2,4)
#else
.decl XYZ2(?type:Type, ?value:Value, ?ctx:Context, ?param:Var)

XYZ2(?type, ?value, ?ctx, ?param) :-
  ParamTaintTransferredToBase(?param, ?ctx, ?base),
  VarPointsTo(_, ?value, ?ctx, ?base),
  Value_isHeap(?value),
  Value_Type(?value, ?type).

TaintedValueTransferred(?declaringType, ?source, ?type, ValueIdMacro(?source, ?type, DEFAULT_BREADCRUMB)),
VarPointsTo(?hctx, ValueIdMacro(?source, ?type, DEFAULT_BREADCRUMB), ?ctx, ?var) :-
  XYZ2(?type, ?oldvalue, ?ctx, ?from),
  TaintedVarPointsTo(?value, ?ctx, ?from),
  AssignNormalHeapAllocation(?oldvalue, ?var, _),
  SourceFromTaintedValue(?value, ?source),
  Value_DeclaringType(?value, ?declaringType),
  isImmutableHContext(?hctx).
#endif

/**
 * Sanitization TODO
 */
#ifdef INFORMATION_FLOW_HIGH_SOUNDNESS
// Option 1
.decl MethodInvocationInMethod(?tomethod:Method, ?invocation:MethodInvocation, ?inmethod:Method)

MethodInvocationInMethod(?tomethod, ?invocation, ?inmethod) :-
  AnyMethodInvocation(?invocation, ?tomethod),
  Instruction_Method(?invocation, ?inmethod).

MethodInvocationInContext(?ctx, ?invocation, ?tomethod) :-
  ReachableContext(?ctx, ?inmethod),
  MethodInvocationInMethod(?tomethod, ?invocation, ?inmethod).
#else
// Option 2
MethodInvocationInContext(?ctx, ?invocation, ?tomethod) :-
  CallGraphEdge(?ctx, ?invocation, _, ?tomethod).
#endif

/**
 * Sinks and leaks
 */
LeakingSinkVariable(?label, ?invocation, ?ctx, ?var) :-
  LeakingSinkMethodArg(?label, ?index, ?tomethod),
  MethodInvocationInContextInApplication(?ctx, ?invocation, ?tomethod),
  ActualParam(?index, ?invocation, ?var).

// in case method has no arguments, assume base variable 
LeakingSinkVariable(?label, ?invocation, ?ctx, ?var) :-
   LeakingSinkMethod(?label, ?tomethod),
   !FormalParam(_, ?tomethod, _),
   MethodInvocationInContextInApplication(?ctx, ?invocation, ?tomethod),
   MethodInvocation_Base(?invocation, ?var).

LeakingTaintedInformation(?sourceLabel, ?destLabel, ?ctx, ?invocation, ?source) :-
  SourceFromTaintedValue(?value, ?source),
  LabelFromSource(?source, ?sourceLabel),
  TaintedVarPointsTo(?value, ?ctx, ?var),
  LeakingSinkVariable(?destLabel, ?invocation, ?ctx, ?var).

/**
 * Special Heap allocation on function call
 */
TaintedValueIntroduced(?declaringType, ?invo, ?type, ?label, ValueIdMacro(?invo, ?type, DEFAULT_BREADCRUMB)),
VarPointsTo(?hctx, ValueIdMacro(?invo, ?type, DEFAULT_BREADCRUMB), ?ctx, ?to) :-
  CallTaintingMethod(?label, ?ctx, ?invo),
  isImmutableHContext(?hctx),
  TypeForReturnValue(?type, ?to, ?invo),
  Instruction_Method(?invo, ?method),
  Method_DeclaringType(?method, ?declaringType).

TaintedVarPointsTo(?value, ?ctx, ?var) :-
  TaintedValue(?value),
  VarPointsTo(_, ?value, ?ctx, ?var).


// Generic string internal tainting
#ifdef INFORMATION_FLOW_HIGH_SOUNDNESS
TaintedValue(?basevalue) :-
  TaintedValue(?value),
  InstanceFieldPointsTo(_, ?value, "<java.lang.String: char[] value>", _, ?basevalue).

TaintedValueTransferred(?declaringType, ?source, "char[]", ValueIdMacro(?source, "char[]", DEFAULT_BREADCRUMB)),
VarPointsTo(?hctx, ValueIdMacro(?source, "char[]", DEFAULT_BREADCRUMB), ?ctx, ?to) :-
  LoadHeapInstanceField(?ctx, ?to, "<java.lang.String: char[] value>", _, ?basevalue),
  TaintedValue(?basevalue),
  Value_DeclaringType(?basevalue, ?declaringType),
  SourceFromTaintedValue(?basevalue, ?source),
  isImmutableHContext(?hctx).
  .plan 1:(2,1,3,4,5)

#endif
 
// Serialization
.decl SerializeObjectInvocation(?base:Var, ?ctx:Context, ?var:Var)

SerializeObjectInvocation(?base, ?ctx, ?var) :-
  MethodInvocationInContextInApplication(?ctx, ?invocation, "<java.io.ObjectOutputStream: void writeObject(java.lang.Object)>"),
  MethodInvocation_Base(?invocation, ?base),
  ActualParam(_, ?invocation, ?var).

.decl ObjectSerializedToBase(?ctx:Context, ?base:Var, ?obj:Value)

ObjectSerializedToBase(?ctx, ?base, ?obj) :-
  SerializeObjectInvocation(?base, ?ctx, ?var),
  VarPointsTo(_, ?obj, ?ctx, ?var),
  SerializableValue(?obj).

.decl SerializableValue(?obj:Value)

SerializableValue(?obj) :-
   Value_Type(?obj, ?type),
   SupertypeOf("java.io.Serializable", ?type).

.decl SerializableVar(?var:Var)

SerializableVar(?var) :-
   Var_Type(?var, ?type),
   SupertypeOf("java.io.Serializable", ?type).

TaintedValueTransferred(?declaringType, ?source, ?type, ValueIdMacro(?source, ?type, DEFAULT_BREADCRUMB)),
VarPointsTo(?hctx, ValueIdMacro(?source, ?type, DEFAULT_BREADCRUMB), ?ctx, ?base) :-
  ObjectSerializedToBase(?ctx, ?base, ?baseobj),
  (InstanceFieldPointsTo(_, ?obj, ?field, _, ?baseobj),
  !Field_Modifier("transient", ?field)),
  TaintedValue(?obj),
  Var_Type(?base, ?type),
  Value_DeclaringType(?obj, ?declaringType),
  SourceFromTaintedValue(?obj, ?source),
  ImmutableHContextFromContext(?ctx, ?hctx).
  .plan 2: (3,2,1,4,5,6,7)

VarIsTaintedFromValue(?type, ?basevalue, ?ctx, ?to) :-
   MaybeDeserializedValue(?basevalue),
   TaintedVarPointsTo(?basevalue, ?ctx, ?base),
   LoadInstanceField(?base, ?sig, ?to, _),
   !Field_Modifier("transient", ?sig),
   Var_Type(?to, ?type).

BaseToRetTaintTransferMethod("<java.io.ObjectInputStream: java.lang.Object readObject()>").

.decl OptDeserializeInvoke(?ret:Var)

OptDeserializeInvoke(?ret) :-
   MethodInvocationInContextInApplication(_, ?invocation, "<java.io.ObjectInputStream: java.lang.Object readObject()>"),
   AssignReturnValue(?invocation, ?ret).

.decl MaybeDeserializedValue(?value:Value)

MaybeDeserializedValue(?value) :-
   OptDeserializeInvoke(?ret),
   TaintedVarPointsTo(?value, _, ?ret),
   SerializableValue(?value).

.decl AppTaintedVar(?var:Var)
.output AppTaintedVar

AppTaintedVar(?var) :-
  TaintedVarPointsTo(_, _, ?var), ApplicationVar(?var).